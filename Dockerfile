FROM node:12-alpine

CMD ["echo", "Starting test"]

RUN apk add --no-cache python2 g++ make

CMD ["echo", "Test complete"]
